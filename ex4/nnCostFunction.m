function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%
% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
Y = zeros(size(y, 1), num_labels);
for r = 1:size(y, 1)
    Y(r, y(r, 1)) = 1.0;
end
m = size(X, 1);
L1 = [ones(m, 1) X];
L2_part = sigmoid(Theta1 * L1');
L2_part = L2_part';
L2 = [ones(m, 1) L2_part];

L3_part = sigmoid(Theta2 * L2');
L3 = L3_part';
fprintf('L3 size: %d %d: ' , size(L3, 1), size(L3, 2));
fprintf('y size : %d %d : ', size(Y, 1), size(Y, 2));
J = sum(sum(log(L3) .* (-Y) - (1-Y) .* log(1-L3))) / m;
J = J + (sum(sum(Theta1(:, 2:end) .^2))+ sum(sum(Theta2(:, 2:end) .^2))) * lambda/m/2;
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
tri_1 = zeros(hidden_layer_size, input_layer_size+1);
tri_2 = zeros(num_labels, hidden_layer_size+1);
for t = 1:m
    a_1 = (X(t,:))';
    a_1 = [1; a_1];
    a_2 = sigmoid(Theta1 * a_1);
    a_2 = [1; a_2];
    a_3 = sigmoid(Theta2 * a_2);
    
    y_t = (Y(t, :))';
    
    delta_3 = a_3 - y_t;
    delta_2 = (Theta2' * delta_3) .* sigmoidGradient([1; Theta1 * a_1]);
    
    tri_1 = tri_1 + delta_2(2:end) * a_1';
    tri_2 = tri_2 + delta_3 * a_2';
    
end 

Theta1_grad = tri_1/m;
Theta2_grad = tri_2/m;
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%
theta1_c = Theta1;
theta1_c(:, 1) = 0;
theta2_c = Theta2;
theta2_c(:, 1) = 0;
Theta1_grad = Theta1_grad + lambda/m * theta1_c;
Theta2_grad = Theta2_grad + lambda/m * theta2_c;

% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
