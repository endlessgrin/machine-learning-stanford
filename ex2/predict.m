function p = predict(theta, X)
%PREDICT Predict whether the label is 0 or 1 using learned logistic 
%regression parameters theta
%   p = PREDICT(theta, X) computes the predictions for X using a 
%   threshold at 0.5 (i.e., if sigmoid(theta'*x) >= 0.5, predict 1)

m = size(X, 1); % Number of training examples

% You need to return the following variables correctly
p = zeros(m, 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned logistic regression parameters. 
%               You should set p to a vector of 0's and 1's
%

prob = sigmoid( X * theta);
% logical indexing
% http://www.mathworks.com/company/newsletters/articles/matrix-indexing-in-matlab.html
% http://stackoverflow.com/questions/11987313/change-elements-of-matrix-based-on-condition
a = prob>=0.5;
b = prob < 0.5;
p(a) = 1;
p(b) = 0;



% =========================================================================


end
